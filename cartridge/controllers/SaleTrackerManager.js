'use strict';

var server = require('server');

var Site = require('dw/system/Site');
var Campaign = require('dw/campaign/Campaign');

const ENTRY = {
    SITE: 'SITE.CURRENT',
    CAMPAING: 'CAMPAING.CURRENT',
    PROMOTION: 'PROMOTION.CURRENT'
};

/**
 * Retrieve site list
 * @returns {array} siteList
 */
function getSiteList() {
    var siteList = [];

    var sites = Site.getAllSites();

    Object.keys(sites).forEach(function (siteId) {
        var site = sites.get(siteId);
        siteList.push({
            id: site.getID(),
            currency: site.getDefaultCurrency()
        });
    });
    
    return siteList;
}

/**
 * Retrieve campaign list
 * @returns {array} campaignList
 */
function getCampaignList() {
    var campaignList = [];

    var sites = Site.getAllSites();

    Object.keys(sites).forEach(function (siteId) {
        var site = sites.get(siteId);
        siteList.push({
            id: site.getID(),
            currency: site.getDefaultCurrency()
        });
    });
    
    return campaignList;
}

/**
 * Retrieve promotion list
 * @returns {array} promotionList
 */
function getPromotionList() {
    var promotionList = [];

    var sites = Site.getAllSites();

    Object.keys(sites).forEach(function (siteId) {
        var site = sites.get(siteId);
        siteList.push({
            id: site.getID(),
            name: site.getName(),
            currency: site.getDefaultCurrency()
        });
    });

    return promotionList;
}

/**
 * Retrieve current site
 * @returns {dw.system.Site}
 */
function getCurrentSiteId() {
    return session.privacy[ENTRY.SITE] || null;
}

/**
 * Retrieve current campaing(s)
 * @returns {array}
 */
function getCurrentCampaing() {
    return session.privacy[ENTRY.SITE] || null;
}

/**
 * Retrieve current campaing(s)
 * @returns {array}
 */
function getCurrentCampaing() {
    return session.privacy[ENTRY.CAMPAING] || null;
}

/**
 * Retrieve current promotion(s)
 * @returns {array}
 */
function getCurrentPromotion() {
    return session.privacy[ENTRY.PROMOTION] || null;
}

server.get('Landing', server.middleware.https, function (req, res, next) {
    var siteList = getSiteList();

    var currentSiteId = getCurrentSiteId();
    var currentCampaign = getCurrentCampaing();
    var currentPromotion = getCurrentPromotion();

    var campaignList = getCampaignList();
    var promotionList = getPromotionList();

    res.render('landing.isml', {
        site_list: siteList,
        current_site_id: currentSiteId,
        campaign_list: campaignList,
        current_ampaign: currentCampaign,
        promotion_list: promotionList,
        current_romotion: currentPromotion,
        landing_url: URLUtils.https('SaleTrackerManager-Landing')
    });

    return next();
});

module.exports = server.exports();