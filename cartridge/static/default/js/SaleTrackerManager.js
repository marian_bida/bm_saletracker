var SaleTrackerManager = (function ($) {
    
    var STATES = {
        ALL: 'all'
    };

    var state = {
        site: null,
        campaign: null,
        promotion: null,
        offset: 0,
        count: 12,
        urls: {
            order: {
                list: '/order/list',
                view: '/order/view'
            }
        },
        totals: {
            total: 0,
            total_compare_to_last_week: 0,
            total_sales_order: 0,
            avg_sales_order: 0
        },
        orders: {
            total: 0,
            orders: []
        },
        init: function () {
            
            this.site = $('[data-action="site:select"].active').attr('data-value');
            this.campaign = $('[data-action="campaign:select"].active').attr('data-value');
            this.promotion = $('[data-action="promotion:select"].active').attr('data-value');
            
            $('body').trigger('state:update');
        },
        setPromotion: function (promotion) {
            this.promotion = promotion;
            
            $('.promotion-list a.active').removeClass('active');
            $('.promotion-list a[data-value="' + this.promotion + '"]').addClass('active');
            
            $('body').trigger('state:update');
        },
        setCampaign: function (campaign) {
            this.campaign = campaign;
            
            $('.campaign-list a.active').removeClass('active');
            $('.campaign-list a[data-value="' + this.campaign + '"]').addClass('active');
            
            this.setPromotion(STATES.ALL);
        },
        setSite: function (site) {
            this.site = site;
            
            $('.site-list a.active').removeClass('active');
            $('.site-list a[data-value="' + this.site + '"]').addClass('active');
            
            this.setCampaign(STATES.ALL);
        },
        getState: function () {
            return {
                site: this.site,
                campaign: this.campaign,
                promotion: this.promotion,
                offset: this.offset,
                count: this.count
            };
        },
        setTotals: function (totals) {
            this.totals = totals;
            
            $('#totals-total').attr('data-value', this.totals.total);
            $('#totals-total-compare-to-last-week').attr('data-value', this.totals.total_compare_to_last_week);
            $('#totals-total-sales-order').attr('data-value', this.totals.total_sales_order);
            $('#totals-avg-sales-order').attr('data-value', this.totals.avg_sales_order);
        },
        setOrders: function (orders) {
            this.orders = orders;
            
            $('#order-total').attr('data-value', this.orders.total);
            
            this.renderOrders();
            
            handleAnimation();
        },
        setPaginationVector: function (vector) {
            switch (vector) {
                case 'first':
                    state.offset = 0;
                    break;
                    
                case 'prev':
                    state.offset = state.offset > 0 ? state.offset - 1 : state.offset;
                    break;
                    
                case 'last':
                    state.offset = Math.floor(state.orders.total / state.count) * state.count;
                    break;
                    
                case 'next':
                    state.offset = (state.offset * state.count) < state.orders.total ? state.offset + 1 : state.offset;
                    break;
                    
                default:
                    state.offset = state.count * parseInt(vector, 10);
            }
            
            $('body').trigger('state:update');
        },
        computePromotionText: function (order) {
            return [order.site, order.campaign, order.promotion].join(' / ');
        },
        renderOrders: function () {
            var computePromotionText = this.computePromotionText;
            $('.list-orders li:not([data-template="true"])').remove();
            
            this.orders.orders.forEach(function (order) {
                var entry = $('.list-orders [data-template="true"]').clone();
                entry.removeAttr('data-template');
                entry.removeClass('d-none');
                
                entry.find('[data-entity="id"]').text(order.id);
                entry.find('[data-entity="promotion"]').text(computePromotionText(order));
                entry.find('[data-entity="date"]').text(order.date);
                entry.find('[data-entity="customer"] img').attr('src', order.customer.image);
                entry.find('[data-entity="total"]').text(order.total);
                
                $('.list-orders').append(entry);
            });
        },
        viewOrder: function (id) {
            alert('view order ' + id + ' not implemented');
        }
    };
    
    function init() {
        $('body').on('state:update', function () {
            var data = $.extend({}, state.getState());
            $.ajax({
                url: state.urls.order.list,
                data: data,
                method: 'POST',
                datatype: 'json'
            }).fail(function (e) {
                $('body').trigger('api:error', e);
            }).done(function (data) {
                if (typeof data.totals !== 'undefined') {
                    state.setTotals(data.totals);
                }
                if (typeof data.orders !== 'undefined') {
                    state.setOrders(data.orders);
                }
            });
        });

        $('body').on('site:select', function (e, data) {
            state.setSite(data.site);
        });

        $('body').on('campaign:select', function (e, data) {
            state.setCampaign(data.campaign);
        });

        $('body').on('promotion:select', function (e, data) {
            state.setPromotion(data.promotion);
        });
        
        $('body').on('pagination:paginate', function (e, data) {
            state.setPaginationVector(data.page);
        });

        $('body').on('click', '[data-action="site:select"]', function (e) {
            e.preventDefault();
            $('body').trigger('site:select', {site: $(this).attr('data-value')});
        });

        $('body').on('click', '[data-action="campaign:select"]', function (e) {
            e.preventDefault();
            $('body').trigger('campaign:select', {campaign: $(this).attr('data-value')});
        });

        $('body').on('click', '[data-action="promotion:select"]', function (e) {
            e.preventDefault();
            $('body').trigger('promotion:select', {promotion: $(this).attr('data-value')});
        });
        
        $('body').on('click', '[data-action="paginate"]', function (e) {
            e.preventDefault();
            $('body').trigger('pagination:paginate', {page: $(this).attr('data-page')});
        });

        state.init();
    }

    return {
        init: function (config) {
            $.extend(this, config);
            $(document).ready(function () {
                init();
            });
        }
    };
}(jQuery));
