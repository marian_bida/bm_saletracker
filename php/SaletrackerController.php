<?php

class SaletrackerController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $req = null, Response $res = null) {
       
        $tpl = $this->model->smarty;

        return $res
            ->setContent($tpl->fetch("saletracker/saletracker.index.tpl"))
            ->getContent();
    }

}
