<?php

class OrderController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $req = null, Response $res = null) {
        return $res
                ->setContent(array(), 'json')
                ->getContent();
    }
    
    public function listAction(Request $req = null, Response $res = null) {
        
        $orderList = array(
            '3512' => array(
                'id' => 3512,
                'site' => 'Jeronimo UK',
                'campaign' => 'Jeronimo Wow',
                'promotion' => 'New Year 20% OFF',
                'date' => '2020-01-12',
                'customer' => array(
                    'name' => 'Marian Bida',
                    'image' => '/assets/img/user/user-1.jpg'
                ),
                'total' => '$10.99'
            ),
            '3514' => array(
                'id' => 3512,
                'site' => 'Jeronimo UK',
                'campaign' => 'Jeronimo Wow',
                'promotion' => 'New Year 20% OFF',
                'date' => '2020-01-12',
                'customer' => array(
                    'name' => 'Peter Smith',
                    'image' => '/assets/img/user/user-2.jpg'
                ),
                'total' => '$11.99'
            ),
            '3513' => array(
                'id' => 3513,
                'site' => 'Jeronimo US',
                'campaign' => 'Jeronimo Friends & Family',
                'promotion' => 'Jeronimo Wow',
                'date' => '2020-01-13',
                'customer' => array(
                    'name' => 'Jeronimo White',
                    'image' => '/assets/img/user/user-3.jpg'
                ),
                'total' => '$12.99'
            )
        );
        
        $totals = array(
            'total' => 0,
            'total_compare_to_last_week' => 0,
            'total_sales_order' => 0,
            'avg_sales_order' => 0
        );
        
        $orders = array(
            'total' => 0,
            'orders' => array()
        );
        
        switch ($req->post['site']) {
            case 'JeronimoUK':
                $totals['total'] = 12345.65;
                $totals['total_compare_to_last_week'] = 12.34;
                $totals['total_sales_order'] = 1233;
                $totals['avg_sales_order'] = 12.34;
                
                $orders['total'] = 26;
                
                $orders['orders'][] = $orderList['3512'];
                $orders['orders'][] = $orderList['3514'];
                break;
            
            case 'JeronimoUS':
                $totals['total'] = .02;
                $totals['total_compare_to_last_week'] = 14.34;
                $totals['total_sales_order'] = 1;
                $totals['avg_sales_order'] = 1;
                
                $orders['total'] = 15;
                
                $orders['orders'][] = $orderList['3513'];
                break;
            
            default:
                $totals['total'] = 12345.67;
                $totals['total_compare_to_last_week'] = 15.46;
                $totals['total_sales_order'] = 1234;
                $totals['avg_sales_order'] = 12.35;
                
                $orders['total'] = 64;
                
                $orders['orders'][] = $orderList['3512'];
                $orders['orders'][] = $orderList['3513'];
                $orders['orders'][] = $orderList['3514'];
        }

        $data = array(
            'state' => true,
            'totals' => $totals,
            'orders' => $orders,
            'req' => array(
                'site' => $req->post['site']
            )
        );
        
        return $res
                ->setContent($data, 'json')
                ->getContent();
    }
    
    public function getAction(Request $req = null, Response $res = null) {
        
        $orderList = array(
            '3512' => array(
                'id' => 3512,
                'site' => 'Jeronimo UK',
                'campaign' => 'Jeronimo Wow',
                'promotion' => 'New Year 20% OFF',
                'date' => '2020-01-12',
                'customer' => array(
                    'name' => 'Marian Bida',
                    'image' => '/assets/img/user/user-1.jpg'
                ),
                'total' => '$10.99'
            ),
            '3514' => array(
                'id' => 3512,
                'site' => 'Jeronimo UK',
                'campaign' => 'Jeronimo Wow',
                'promotion' => 'New Year 20% OFF',
                'date' => '2020-01-12',
                'customer' => array(
                    'name' => 'Peter Smith',
                    'image' => '/assets/img/user/user-2.jpg'
                ),
                'total' => '$11.99'
            ),
            '3513' => array(
                'id' => 3513,
                'site' => 'Jeronimo US',
                'campaign' => 'Jeronimo Friends & Family',
                'promotion' => 'Jeronimo Wow',
                'date' => '2020-01-13',
                'customer' => array(
                    'name' => 'Jeronimo White',
                    'image' => '/assets/img/user/user-3.jpg'
                ),
                'total' => '$12.99'
            )
        );
        
        $data = $orderList['3512'];
        
        return $res
                ->setContent($data, 'json')
                ->getContent();
    }
}
