<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Sale Tracker</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="../assets/css/default/app.min.css" rel="stylesheet" />
    <link href="../assets/css/default/saletracker.css" rel="stylesheet" />
</head>
<body>
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <div id="header" class="header navbar-default">
            <div class="navbar-header">
                <a href="/" class="navbar-brand"><span class="navbar-logo"></span> <b>Sale</b> Tracker</a>
            </div>
        </div>
        <div id="content" class="content">
            <h1 class="page-header mb-3">Sale Tracker</h1>
            <div class="sale-tracker-wrap">
                <div class="head row">
                    <div class="col-xl-6">
                        <div class="card border-0 mb-3 ">
                            <div class="card-body">
                                <ul class="breadcrumb site-list">
                                    <li class="breadcrumb-item" data-template="true">
                                        <a
                                            href="javascript:" 
                                            data-action="site:select" 
                                            data-value="all" 
                                            class="btn btn-primary btn-xs active"
                                            title="All">
                                            All Sites
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a
                                            href="javascript:;" 
                                            data-action="site:select" 
                                            data-value="JeronimoUK"
                                            class="btn btn-primary btn-xs" 
                                            title="">
                                            Jeronimo UK</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a
                                            href="javascript:;" 
                                            data-action="site:select" 
                                            data-value="JeronimoUS"
                                            class="btn btn-primary btn-xs" 
                                            title="">
                                            Jeronimo US</a>
                                    </li>
                                </ul>
                                <ul class="breadcrumb campaign-list">
                                    <li class="breadcrumb-item" data-template="true">
                                        <a
                                            href="javascript:" 
                                            data-action="campaign:select" 
                                            data-value="all" 
                                            class="btn btn-primary btn-xs active" 
                                            title="">
                                            All Campaigns
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a
                                            href="javascript:" 
                                            data-action="campaign:select" 
                                            data-value="JeronimoWow" 
                                            class="btn btn-primary btn-xs" 
                                            title="">
                                            Jeronimo Wow
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a
                                            href="javascript:" 
                                            data-action="campaign:select" 
                                            data-value="JeronimoFriendsFamily" 
                                            class="btn btn-primary btn-xs" 
                                            title="">
                                            Jeronimo Friends & Family
                                        </a>
                                    </li>
                                </ul>
                                <ul class="breadcrumb promotion-list">
                                    <li class="breadcrumb-item" data-template="true">
                                        <a
                                            href="javascript:" 
                                            data-action="promotion:select" 
                                            data-value="all" 
                                            class="btn btn-primary btn-xs active" 
                                            title="All">
                                            All Promotions
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a
                                            href="javascript:" 
                                            data-action="promotion:select" 
                                            data-value="NewYear20%OFF" 
                                            class="btn btn-primary btn-xs" 
                                            title="New Year 20% OFF">
                                            New Year 20% OFF
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a
                                            href="javascript:" 
                                            data-action="promotion:select" 
                                            data-value="Christmass30%OFF" 
                                            class="btn btn-primary btn-xs" 
                                            title="Christmass30%OFF">
                                            Christmass 30% OFF
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card border-0 mb-3 overflow-hidden bg-dark text-white">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="mb-3 text-grey">
                                            <b>TOTAL SALES</b>
                                            <span class="ml-2">
                                                <i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-title="Total sales" data-placement="top" data-content="Net sales (gross sales minus discounts and returns) plus taxes and shipping. Includes orders from all sales channels."></i>
                                            </span>
                                        </div>
                                        <div class="d-flex mb-1">
                                            <h2 class="mb-0">$<span id="totals-total" data-animation="number" data-value="0">0.00</span></h2>
                                            <div class="ml-auto mt-n1 mb-n1"><div id="total-sales-sparkline"></div></div>
                                        </div>
                                        <div class="mb-3 text-grey">
                                            <i class="fa fa-caret-up"></i> <span id="totals-total-compare-to-last-week" data-animation="number" data-value="12.34">0.00</span>% compare to last week
                                        </div>
                                        <hr class="bg-white-transparent-2" />
                                        <div class="row text-truncate">
                                            <div class="col-6">
                                                <div class="f-s-12 text-grey">Total sales order</div>
                                                <div class="f-s-18 m-b-5 f-w-600 p-b-1" id="totals-total-sales-order" data-animation="number" data-value="1234">0</div>
                                                <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                                    <div class="progress-bar progress-bar-striped rounded-right bg-teal" data-animation="width" data-value="55%" style="width: 0%"></div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="f-s-12 text-grey">Avg. sales per order</div>
                                                <div class="f-s-18 m-b-5 f-w-600 p-b-1">$<span id="totals-avg-sales-order" data-animation="number" data-value="12.34">0.00</span></div>
                                                <div class="progress progress-xs rounded-lg bg-dark-darker m-b-5">
                                                    <div class="progress-bar progress-bar-striped rounded-right" data-animation="width" data-value="55%" style="width: 0%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-lg-4 align-items-center d-flex justify-content-center">
                                        <img src="../assets/img/svg/img-1.svg" height="150px" class="d-none d-lg-block" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inbox bg-light">
                    <div class="bg-white">
                        <ul class="list-group list-group-lg no-radius list-email list-orders">
                            <li class="list-group-item d-none" data-template="true">
                                <span class="about" data-entity="id"></span>
                                <span class="promotion" data-entity="promotion"></span>
                                <span class="date" data-entity="date"></span>
                                <span class="customer email-user" data-entity="customer">
                                    <img src="" alt="" />
                                </span>
                                <span class="total" data-entity="total"></span>
                            </li>
                        </ul>
                    </div>
                    <div class="wrapper clearfix d-flex align-items-center">
                        <div class="text-inverse f-w-600">
                            <span id="order-total" data-animation="number" data-value="0"></span> orders
                        </div>
                        <div class="btn-group ml-auto order-pagination">
                            <button class="btn btn-white btn-sm" data-template="true">
                                <i class="fa fa-fw fa-angle-double-left" data-action="paginate" data-page="first"></i>
                            </button>
                            <button class="btn btn-white btn-sm" data-template="true">
                                <i class="fa fa-fw fa-angle-left" data-action="paginate" data-page="prev"></i>
                            </button>
                            <button class="btn btn-white btn-sm" data-action="paginate" data-page="1" data-template="true">
                                1
                            </button>
                            <button class="btn btn-white btn-sm" data-action="paginate" data-page="2">
                                2
                            </button>
                            <button class="btn btn-white btn-sm" data-action="paginate" data-page="3">
                                3
                            </button>
                            <button class="btn btn-white btn-sm" data-action="paginate" data-page="4">
                                4
                            </button>
                            <button class="btn btn-white btn-sm" data-action="paginate" data-page="5">
                                5
                            </button>
                            <button class="btn btn-white btn-sm" data-template="true">
                                <i class="fa fa-fw fa-angle-right" data-action="paginate" data-page="next"></i>
                            </button>
                            <button class="btn btn-white btn-sm" data-template="true">
                                <i class="fa fa-fw fa-angle-double-right" data-action="paginate" data-page="last"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="footer"></div>
            </div>
        </div>
    </div>

    <script src="../assets/js/app.min.js"></script>
    <script src="../assets/js/theme/default.js"></script>
    <script src="../assets/js/sale-tracker-manager.js"></script>

    <script>
    $(document).ready(function () {
        SaleTrackerManager.init();
    });
    </script>
</body>
</html>